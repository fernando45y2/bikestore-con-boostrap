import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BikeCreateComponent} from './bike-create/bike-create.component';
import {BikeListComponent} from './bike-list/bike-list.component';
import {BikeUpdateComponent} from './bike-update/bike-update.component';


const routes: Routes = [
  {
    path: '',
    component: BikeCreateComponent
  },
  {
    path: 'bike-create',
    component: BikeCreateComponent
  },
  {
    path: 'bike-list',
    component: BikeListComponent
  },
  {
    path: 'bike-update/:id',
    component: BikeUpdateComponent
  },
  {
    path: 'bike-update',
    component: BikeUpdateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BikeRoutingModule {
}
