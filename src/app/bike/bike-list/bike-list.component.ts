import {Component, OnInit} from '@angular/core';
import {Bike} from '../bike';
import {ServiceService} from '../service.service';

@Component({
  selector: 'app-bike-list',
  templateUrl: './bike-list.component.html',
  styleUrls: ['./bike-list.component.css']
})
export class BikeListComponent implements OnInit {

  public bikeList: Bike[];

  constructor(private bikeServe: ServiceService) {
  }

  ngOnInit() {
    this.bikeServe.queryBikes()
      .subscribe(res => {
          this.bikeList = res;
          console.log('response data', res);
        },
        error => console.error('error', error)
      );
  }


}
