import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';
import {environment} from 'src/environments/environment'
import {Bike} from './bike';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  /**este metodo es para obtener toodos los datos que agrego en mi g */

  constructor(private http: HttpClient) {
  }


  public queryBikes(): Observable<Bike[]> {
    return this.http.get<Bike[]>(`${environment.END_POINT}/api/bikes`)
      .pipe(map(res => {
        return res;
      }));
  }


  /**este metodo es para guardar todo lo que escribo de mi html a el backen */
  public saveBike(bike: Bike): Observable<Bike> {
    return this.http.post<Bike>(`${environment.END_POINT}/api/bikes`, bike)
      .pipe(map(res => {
        return res;
      }));

  }

  public getBikeById(id: String): Observable<Bike> {
    return this.http.get<Bike>(`${environment.END_POINT}/api/bikes/${id}`)
      .pipe(map(res => {
        return res;
      }));

  }

  public updateBike(bike: Bike): Observable<Bike> {
    return this.http.put<Bike>(`${environment.END_POINT}/api/bikes`, bike)
      .pipe(map(res => {
        return res;
      }))
  }

}
