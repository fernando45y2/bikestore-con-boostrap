import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ServiceService} from '../service.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Bike} from '../bike';

@Component({
  selector: 'app-bike-update',
  templateUrl: './bike-update.component.html',
  styleUrls: ['./bike-update.component.css']
})
export class BikeUpdateComponent implements OnInit {

  bikeFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private serviceservice: ServiceService,
              private activeteRoute: ActivatedRoute, private router: Router) {

    this.bikeFormGroup = this.formBuilder.group({

      id: [''],
      model: ['', Validators.compose([Validators.required, Validators.maxLength(5)])],
      price: [''],
      serial: ['']
    })

  }

  ngOnInit() {

    let id = this.activeteRoute.snapshot.paramMap.get('id');
    console.log('ID path', id);
    this.serviceservice.getBikeById(id)
      .subscribe(res => {
        console.log('get data ok', res);
        this.loadForm(res);
      });
  }

  saveBike() {
    console.log('Datos', this.bikeFormGroup.value);
    this.serviceservice.updateBike(this.bikeFormGroup.value)
      .subscribe(res => {
        console.log('update ok', res);
        this.router.navigate(['/bike/bike-list'])
      }, error => {
        console.error('Error', error);
      });
  }

  private loadForm(bike: Bike) {
    this.bikeFormGroup.patchValue({
      id: bike.id,
      model: bike.model,
      price: bike.price,
      seriar: bike.serial
    });
  }

}
