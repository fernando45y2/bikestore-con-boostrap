import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BikeRoutingModule} from './bike-routing.module';
import {BikeListComponent} from './bike-list/bike-list.component';
import {BikeCreateComponent} from './bike-create/bike-create.component';
import {BikeUpdateComponent} from './bike-update/bike-update.component';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [BikeListComponent, BikeCreateComponent, BikeUpdateComponent],
  imports: [
    CommonModule,
    BikeRoutingModule,
    ReactiveFormsModule


  ]
})
export class BikeModule {
}
