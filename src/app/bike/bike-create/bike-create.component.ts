import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ServiceService} from '../service.service';

@Component({
  selector: 'app-bike-create',
  templateUrl: './bike-create.component.html',
  styleUrls: ['./bike-create.component.css']
})
export class BikeCreateComponent implements OnInit {
  bikeFormGroup: FormGroup;

  constructor(private formbuilder: FormBuilder, private serviceService: ServiceService) {
    this.bikeFormGroup = this.formbuilder.group({
      model: ['', Validators.compose([Validators.required, Validators.maxLength(5)])],
      price: [''],
      serial: ['']
    });
  }

  ngOnInit() {
  }

  /**este metodo es poara guardar la bicicleta que añada en mi html */
  saveBike() {
    console.log('DATOS', this.bikeFormGroup.value);
    this.serviceService.saveBike(this.bikeFormGroup.value)
      .subscribe(res => {
        console.warn('SAVE OK', res);
      }, error => {
        console.error('ERROR', error);
      });
  }

}
