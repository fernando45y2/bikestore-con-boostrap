import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Salesinterface} from './salesinterface';
import {environment} from 'src/environments/environment';
import {map} from 'rxjs/operators';
import {Bike} from '../bike/bike';
import {Clientinterface} from '../clients/clientinterface';


@Injectable({
  providedIn: 'root'
})
export class SaleService {

  constructor(private http: HttpClient) {
  }

  public query(): Observable<Salesinterface[]> {
    return this.http.get<Salesinterface[]>(`${environment.END_POINT}/api/sales`)
      .pipe(map(res => {
        return res;
      }));

  }


  public querybikes(): Observable<Bike[]> {
    return this.http.get<Bike[]>(`${environment.END_POINT}/api/bikes`)
      .pipe(map(res => {
        return res;
      }));
  }

  public getClientByDocument(document: string): Observable<Clientinterface> {
    let params = new HttpParams();
    params = params.append('document', document);
    console.warn('PARAMS', params);
    return this.http.get<Clientinterface>(`${environment.END_POINT}/api/clients/find-client-by-document`, {params: params})
      .pipe(map(res => {
        return res;
      }));
  }

  public saveCient(clientInterface: Clientinterface): Observable<Clientinterface> {
    return this.http.post<Clientinterface>(`${environment.END_POINT}/api/bikes`, clientInterface)
      .pipe(map(res => {
        return res;
      }));
  }


}
