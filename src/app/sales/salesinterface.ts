import {Bike} from '../bike/bike';

export interface Salesinterface {
  id?: Number;
  date?: Date;
  clientId?: Number;
  clientName?: String;
  bikesId?: Number;
  bikeSerial?: String;
  bikesPrice?: Number;
  total?: Number;

}
