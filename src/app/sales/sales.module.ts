import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SalesRoutingModule} from './sales-routing.module';
import {SaleListComponent} from './sale-list/sale-list.component';
import {SaleCreateComponent} from './sale-create/sale-create.component';
import {SaleUpdateComponent} from './sale-update/sale-update.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MainSalesComponent} from './main-sales/main-sales.component';


@NgModule({
  declarations: [SaleListComponent, SaleCreateComponent, SaleUpdateComponent, MainSalesComponent],
  imports: [
    CommonModule,
    SalesRoutingModule,
    ReactiveFormsModule
  ]
})
export class SalesModule {
}
