import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SaleListComponent} from './sale-list/sale-list.component';
import {SaleCreateComponent} from './sale-create/sale-create.component';
import {SaleUpdateComponent} from './sale-update/sale-update.component';
import {MainSalesComponent} from './main-sales/main-sales.component';


const routes: Routes = [
  {
    path: '',
    component: MainSalesComponent,
    children: [
      {
        path: 'sale-list',
        component: SaleListComponent
      },
      {
        path: 'sale-create',
        component: SaleCreateComponent
      },
      {
        path: 'sale-update',
        component: SaleUpdateComponent
      }
    ]
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesRoutingModule {
}
