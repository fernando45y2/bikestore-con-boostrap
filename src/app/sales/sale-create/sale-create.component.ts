import {Component, OnInit} from '@angular/core';
import {Bike} from 'src/app/bike/bike';
import {ServiceService} from 'src/app/bike/service.service';
import {Salesinterface} from '../salesinterface';
import {SaleService} from '../sale.service';
import {ClientService} from 'src/app/clients/client.service';
import {Clientinterface} from 'src/app/clients/clientinterface';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-sale-create',
  templateUrl: './sale-create.component.html',
  styleUrls: ['./sale-create.component.css']
})
export class SaleCreateComponent implements OnInit {
  /**aqui añado las interfazes que voy a usar en mi codigo */
  saleFormGroup: FormGroup;
  salelist: Bike[];
  saleListatCar: Salesinterface[] = [];
  clientlisttemp: Clientinterface [];
  clientdocument: Clientinterface;
  clientlist: Clientinterface[];
  /**en esta parte estoy creando una variable a la cual la inicio en 0 */
  suma: number = 0;

  statusSearchDocument: boolean = false;

  SearchForm = this.formBuilder.group({
    document: ''
  })


  constructor(
    /**en esta parte añado los  servicios que voy a usar  */
    private bikeserve: ServiceService,
    private salesServie: SaleService,
    private clienservice: ClientService,
    private formBuilder: FormBuilder,
  ) {

  }

  /**En esta parte añado la interfaz de Bike */
  ngOnInit(): void {
    this.bikeserve.queryBikes()
      .subscribe(res => {
        this.salelist = res;
      });
    /**en esta parte añado la interfaz de client */
    this.clienservice.query()
      .subscribe(res => {
        this.clientlisttemp = res;
      });
  }


  /**en esta parte creo el metodo de añadir el carrito de la interfaz bike, y añado lo que voy a mostrar  */


  addToCar(item: Bike): void {
    console.warn('selected', item);
    /**en esta parte uso mi variable de "suma" y la sumo con el item.price  */
    this.suma = this.suma + item.price
    this.saleListatCar.push({

      bikeSerial: item.serial,
      bikesPrice: item.price
    });
  }


  /**En esta parte añado el metodo para buscar el cliente por el numero de documento  */


}
