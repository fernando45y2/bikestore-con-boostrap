import {Component, OnInit} from '@angular/core';
import {SaleService} from '../sale.service';
import {Salesinterface} from '../salesinterface';

@Component({
  selector: 'app-sale-list',
  templateUrl: './sale-list.component.html',
  styleUrls: ['./sale-list.component.css']
})
export class SaleListComponent implements OnInit {

  public salelist: Salesinterface[];

  constructor(private saleservice: SaleService) {
  }

  ngOnInit() {
    this.saleservice.query()
      .subscribe(res => {
          this.salelist = res;
          console.log('response data', res);
        },
        error => console.error('error', error)
      );
  }

  saveSale() {
    console.log('Datos')
  }

}
