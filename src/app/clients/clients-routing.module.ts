import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ClientListComponent} from './client-list/client-list.component';
import {ClientCreateComponent} from './client-create/client-create.component';
import {ClientUpdateComponent} from './client-update/client-update.component';


const routes: Routes = [
  {
    path: "client-list",
    component: ClientListComponent

  },
  {
    path: "client-create",
    component: ClientCreateComponent
  },
  {
    path: "client-uodate",
    component: ClientUpdateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule {
}
