import {Component, OnInit} from '@angular/core';
import {Clientinterface} from '../clientinterface';
import {ClientService} from '../client.service';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit {

  public clientlist: Clientinterface[];

  constructor(private clientService: ClientService) {
  }

  ngOnInit() {
    this.clientService.query()
      .subscribe(res => {
          this.clientlist = res;
          console.log('response data', res);
        },
        error => console.error('error', error)
      );
  }

  saveclients() {
    console.log('datos')
  }

}
