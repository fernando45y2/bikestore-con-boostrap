import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Clientinterface} from './clientinterface';
import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs/internal/Observable';
import {map} from 'rxjs/operators';
import {createRequestParams} from '../utils/request.utils';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) {
  }

  public query(): Observable<Clientinterface[]> {

    let params = createRequestParams();
    return this.http.get<Clientinterface[]>(`${environment.END_POINT}/api/clients`)
      .pipe(map(res => {
        return res;
      }));
  }

  /*
  public getClientByDocument(document: string): Observable<Clientinterface>{
    let params = new HttpParams();

    params = params.append('document.contains', document);
    /**se pueden filtral por otros

    console.warn('PARAMS', params);
    return this.http.get<Clientinterface>(`${environment.END_POINT}/api/clients/find-client-by-document`,{params: params})
    .pipe(map(res =>{
      return res;
    }));
  }
  */
}
