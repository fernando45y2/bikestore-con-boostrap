import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ICredentials, Credentials } from '../credentials';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { error } from 'protractor';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
    remerberMe: new FormControl('')


  })

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }
  onLogin(): void {

    const credentials: ICredentials = new Credentials();
    credentials.username = this.loginForm.value.username;
    credentials.password = this.loginForm.value.password;
    credentials.remerberMe = this.loginForm.value.remerberMe;

    this.authService.login(credentials)
    .subscribe((res: any) =>{
      this.router.navigate(['/main'])

      console.warn('Data response controller', res);
    },(error: any) => {
      if (error.status === 401){
        console.warn('usuario o contraseña inconrrecta');
      }
    })
  }

}
