import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICredentials } from './credentials';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  public login(credentials: ICredentials): Observable<any>{
    return this.http.post<any>(`${environment.END_POINT}/api/authenticate`, credentials, {observe: 'response'})
    .pipe(map(res =>{
      console.warn('View Response', res)
      const bearerToken = res.headers.get('Authorization');
      console.warn('bearer Token', bearerToken);
      if (bearerToken && bearerToken.slice(0, 7) === 'Bearer') {
        const jtw = bearerToken.slice(7, bearerToken.length);
        this.storeAuthenticationToken(jtw, credentials.remerberMe)
        return jtw;

      }
    }));
  }

  private storeAuthenticationToken(jtw: string, remerberMe: boolean): void{

    if (remerberMe) {
      localStorage.setItem('token', jtw);
    } else{
      sessionStorage.setItem('token', jtw);

    }
  }

}
