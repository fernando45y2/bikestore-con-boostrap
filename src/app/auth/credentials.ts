export interface ICredentials {
  username?: string;
  password?: string;
  remerberMe?: boolean;
}

export class Credentials implements ICredentials{
  constructor(
     public username?: string,
     public password?: string,
     public remerberMe?: boolean
  ){}
}
