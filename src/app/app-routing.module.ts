import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { MainComponent } from './common/main/main.component';
import { HeaderComponent } from './header/header.component';



const routes: Routes = [
  {
    path: 'bike',
    loadChildren: () => import('./bike/bike.module')
      .then(modulo => modulo.BikeModule)
  },
  {
    path: 'sales',
    loadChildren: () => import('./sales/sales.module')
      .then(modulo => modulo.SalesModule)
  },
  {
    path: 'clients',
    loadChildren: () => import('./clients/clients.module')
      .then(modulo => modulo.ClientsModule)
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
path:'main',
component:MainComponent,
children:[
  {
    path:'header',
    component: HeaderComponent
  }
]

  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
